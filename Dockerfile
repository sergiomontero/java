FROM openjdk:8-jre

ENTRYPOINT ["/usr/bin/java", "-jar", "/usr/local/javapkg/entrypoint.jar"]